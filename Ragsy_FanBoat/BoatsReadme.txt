Experimental work in progress version 1.


Launch

Place the Fanboat on/in water in a 'shallow water' area that has fairly flat ground underneath

Controls

Steering is A and D as per vanilla settings ....mouse is for looking around only and has no affect on steering a water vehicle.
Space bar and c control the up and down angles (same as a gyro) which you may occasionally need to adjust slightly as you travel on water otherwise you may sink.  
W is forwards and S is slow down/reverse ..If you let go of w and shift (Turbo) then the Fanboat slowly comes to a stop, pressing s will slow the Fanboat down
more quickly this replaces spacebar as the brake key.

To anchor the Fanboat and stop it drifting away use c and spacebar to level the Fanboat till it stops drifting.

Particle system added...

The Fanboat can be picked up into inventory, if you sink it at any point, although be careful if you sink it too far away from the shores as it could be a long swim back. 
Zombies now can attack water vehicles in game.

The Fanboat is craftable in a standard workbench with a high progression needed, you may find certain parts/assemblies in loot to help get you started.

Happy Boating in A19

Ragsy!!

 

